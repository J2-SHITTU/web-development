import mysql.connector
from mysql.connector import errorcode
from flask import Flask, session, render_template, url_for, redirect, flash, request, jsonify
from passlib.hash import sha256_crypt
# import numpy as np
# import hashlib
from datetime import datetime, timedelta
import gc
from functools import wraps


epoch = datetime(1970, 1, 1)


def timestamp_microsecond(clock):
    td = clock - epoch
    assert td.resolution == timedelta(microseconds=1)
    return (td.days * 86400 + td.seconds) * 10**6 + td.microseconds

timenow = timestamp_microsecond(datetime.now())



app = Flask(__name__) 
def getConnection():
    try:
        conn  = mysql.connector.connect(
        host = "127.0.0.1",
        username = "root",
        password = "#Haryo_x#2" 
    )

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Username or Password is not working")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    

    else:
        return conn
    
app.secret_key = b"33_35A#VD\nxbcd$$##/"
app.secret_key = "qwertyuiop123"

DB_NAME ="test"

@app.route("/")
@app.route("/departurecity",methods=['POST', 'GET'])
def departurecity():
    conn = getConnection()

    if conn != None:    #Checking if connection is None         
            print('MySQL Connection is established')
            dbcursor = conn.cursor()    #Creating cursor object
            dbcursor.execute("USE {}".format(DB_NAME))                          
            SQL_statement = ("SELECT DISTINCT DEPARTURE FROM `HORIZON_TRAVELS_FOR_AIR`;") 
		    #print('SELECT statement executed successfully.')  
            dbcursor.execute(SQL_statement)          
            rows = dbcursor.fetchall()                                                
            conn.close() #Connection must be closed
            cities = []
            for city in rows:
                city = str(city).strip("(")
                city = str(city).strip(")")
                city = str(city).strip(",")
                city = str(city).strip("'")
                cities.append(city)
              
            return render_template("log in and sign up page.html", departureslist=cities)
    else:
            print("DB connection Error")
            return "DB Connection Error"
        
        
# @app.route("/sqlstatement")
# def sqlstate():
#     return render_template("sql.html")

@app.route("/update_password")
def updtpass():
    return render_template("updatepassword.html")


@app.route("/admin")
def admin():
    return render_template("admin.html")

@app.route("/cancel_booking")
def cancel_booking():
    return render_template("cancel_booking.html")

@app.route("/cancellationfee")
def cancellation_fee():
    return render_template("cancellation_fee.html")

@app.route("/v_or_c_bookings")
def v_or_c_bookings():
    return render_template("view_or_cancel_booking.html")

@app.route("/v_or_c_bookings2", methods=["POST", "GET"])
def v_or_c_bookings2():
    if request.method == "POST":
        return render_template("view_or_cancel_booking.html")

@app.route("/delete_account")
def delete_account():
    return render_template("delete_account.html")
  
# @app.route("/user/<user>")
# def for_user(user):
#     if user == "Jubril":
#         return render_template("admin.html", user = user)
#     else:
#         return redirect(url_for("log_in_and_sign_up"))

@app.route("/loginandsignuppage")
def log_in_and_sign_up():
    return render_template("log in and sign up page.html")

@app.route("/TermsandCondition")
def T_C():
    return render_template ("Terms and Condition.html")

@app.route("/loginpage2")
def log_in_page_2():
    return render_template("loginpage2.html")

@app.route("/Signuppage")
def sign_up_page():
    return render_template("Sign up page.html")

@app.route("/Returnticket")
def return_ticket_page():
    return (render_template("Return ticket.html"))

@app.route("/Onewayticket", methods =['POST', 'GET'])
def one_way_ticket(): 
    # departcity = request.form['departureslist']
    # arrivalcity = request.form['arrivalslist']  
    # adultseats = request.form['adultseats']
    # childseats = request.form['childseats']

    conn = getConnection()
    if conn != None:    #Checking if connection is None                    
                    if conn.is_connected(): #Checking if connection is established                        
                        print('MySQL Connection is established')                          
                        dbcursor = conn.cursor()    #Creating cursor object   
                        dbcursor.execute ("USE {}".format(DB_NAME)  ) 
                        SQL_statement = ("SELECT DISTINCT DEPARTURE FROM `HORIZON_TRAVELS_FOR_AIR`;") 
		                #print('SELECT statement executed successfully.')  
                        dbcursor.execute(SQL_statement)          
                        rows = dbcursor.fetchall()                                    
                        dbcursor.close()              
                        conn.close() #Connection must be closed
                        cities = []
                        for city in rows:
                            city = str(city).strip("(")
                            city = str(city).strip(")")
                            city = str(city).strip(",")
                            city = str(city).strip("'")
                            cities.append(city)
                        
                        return(render_template("One way ticket.html",departureslist=cities))
                        # totalseats = int(adultseats) + int(childseats)

                        # dbcursor.execute('SELECT * FROM `HORIZON_TRAVELS_FOR_AIR` LEFT JOIN `SEATS_TABLE` ON TRAVEL_ID = SEAT_ID\
                        #          WHERE DEPARTURE = %s AND ARRIVAL = %s;',(departcity, arrivalcity) )
                        # print("SELECT STATEMENT SUCCESSFUL")
                        # rows2 = dbcursor.fetchone()
                        # seats_available = int(rows2[7])
                        # print(seats_available)
                        # seats_left = (int(seats_available) - totalseats)
                        # print(seats_left)

                        # if (seats_left < 0): 
                        #     return(render_template("One way ticket.html",departureslist=cities, message="Sorry. No seats availaible anymore"))

                            

# @app.route("/Traintravel")
# def train_travel():
#     return(render_template("Train travel.html"))

# @app.route("/Airtravel")
# def air_travel():
#     return(render_template("Air travel.html"))

# @app.route("/Coachtravel")
# def Coach_travel():
#     return(render_template("Coach travel.html"))

# @app.route("/D&Dforcoach")
# def dd_for_coach():
#     return(render_template("D&D for coach.html"))

# @app.route("/D&Dfortrain")
# def dd_for_train():
#     return(render_template("D&D for train.html"))

# @app.route("/R&Oforplane")
# def ro_for_plane():
#     return(render_template("R&O for plane.html"))

# @app.route("/Transportselectionpage")
# def transport_selection():
#     return(render_template("Transport selection page.html"))

@app.route("/Forgotpasswordpage")
def forgotten_password():
    return(render_template("Forgot password page.html"))

@app.route("/bookingsuccesspage")
def booking_success_page():
    return render_template("booking success page.html")

@app.route('/signup/', methods=['POST', 'GET'])
def sign_up():
    error = ''
    print('Register start')
    try:
        if request.method == "POST":         
            username = request.form['username']
            password = request.form['password']
            email = request.form['email']                      
            if username != None and password != None and email != None:           
                conn = getConnection()
                if conn != None:    #Checking if connection is None           
                    if conn.is_connected(): #Checking if connection is established
                        print('MySQL Connection is established')                          
                        dbcursor = conn.cursor()    #Creating cursor object 
                        #here we should check if username / email already exists                                                           
                        password = sha256_crypt.hash((str(password))) 
                        dbcursor.execute ("USE {}". format(DB_NAME))          
                        sql_Query = "SELECT * FROM USERS WHERE username = %s;"
                        dbcursor.execute(sql_Query,(username,))
                        rows = dbcursor.fetchall()           
                        if dbcursor.rowcount > 0:   #this means there is a user with same name
                            print('Username already taken, please choose another')
                            error = "Username already taken, please choose another"
                            return render_template("Sign up page.html", error=error)    
                        else:   #this means we can add new user             
                            dbcursor.execute("INSERT INTO USERS (username, password_hash, \
                                 email) VALUES (%s, %s, %s)", (username, password, email))                
                            conn.commit()  #saves data in database              
                            print("Thanks for signing up!")
                            dbcursor.close()
                            conn.close()
                            gc.collect()                        
                            session['logged_in'] = True     #session variables
                            session['username'] = username
                            session['usertype'] = 'standard'   #default all users are standard
                            return redirect(url_for("one_way_ticket",message='You have been registered successfully!'))
                    else:                        
                        print('Connection error')
                        return 'DB Connection Error'
                else:                    
                    print('Connection error')
                    return 'DB Connection Error'
            else:                
                print('empty parameters')
                return render_template("Sign up page.html", error=error)
        else:            
            return render_template("Sign up page.html", error=error)        
    except Exception as e:                
        return render_template("Sign up page.html", error=e)    

@app.route("/Loginpage", methods=['POST','GET'])
def login():
    form={}
    error = ''
    try:	
        if request.method == "POST":            
            username = request.form['username']
            password = request.form['password']            
            form = request.form
            
            if username != None and password != None:  #check if un or pw is none          
                conn = getConnection()
                if conn != None:    #Checking if connection is None                    
                    if conn.is_connected(): #Checking if connection is established                        
                        print('MySQL Connection is established')                          
                        dbcursor = conn.cursor()    #Creating cursor object   
                        dbcursor.execute ("USE {}".format(DB_NAME)  )    
                        dbcursor.execute("SELECT password_hash, usertype \
                            FROM USERS WHERE username = %s;", (username,))                                              
                        data = dbcursor.fetchone()
                        
                        if dbcursor.rowcount < 1: #this mean no user exists                         
                            error = "User / password does not exist, try logging in again"
                            return render_template("Log in page.html", error=error)

                        else:                            
                            #data = dbcursor.fetchone()[0] #extracting password   
                            # verify passowrd hash and password received from user                                                             
                            if sha256_crypt.verify(request.form['password'], str(data[0])):                                
                                session['logged_in'] = True     #set session variables
                                session['username'] = request.form['username']
                                session['usertype'] = str(data[1])                          
                                print("You are now logged in")
                                if session['usertype'] == "admin":                               
                                    return render_template("admin.html")
                                else:
                                    return redirect(url_for("one_way_ticket"))
                            else:
                                error = "Invalid credentials username/password, try again." 
                                return render_template("Log in page.html", error=error)                              
                                gc.collect()
                    else:                    
                        print('Connection error')
                else:
                    return render_template("Log in page.html", form=form, error=error)
            else:            
                return render_template("Log in page.html", error=error)
        else:
            return render_template("Log in page.html", error = error)
    except Exception as e:                
        error = str(e) + " <br/> Invalid credentials, try again."
        return render_template("Log in page.html", form = form, error = error)   
  

@app.route('/retrieve_travel')
def retrieve_travel():
    conn = getConnection()
    if conn != None:    #Checking if connection is None
        if conn.is_connected(): #Checking if connection is established
            print('MySQL Connection is established')                          
            dbcursor = conn.cursor()    #Creating cursor object  
            dbcursor.execute("USE {}".format(DB_NAME))          
            SQL_statement = "SELECT TRAVEL_ID from `HORIZON_TRAVELS_FOR_AIR`; "          
            dbcursor.execute(SQL_statement)
            print('SELECT statement executed successfully.')             
            rows = dbcursor.fetchall()                                    
            dbcursor.close()              
            conn.close() #Connection must be closed
            return render_template('retrieving_data.html', resultset=rows)
        else:
            print('DB connection Error')
            return 'DB Connection Error'
    else:
        print('DB Connection Error')
        return 'DB Connection Error'   

@app.route('/show_travel', methods=['POST', 'GET'])
def show_travel():
    #fetch from database with where clause  
    if request.method == 'GET':
        travelid = request.args.get('travelid')
        if travelid != None:
            conn = getConnection()
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()    #Creating cursor object  
                    dbcursor.execute ("USE {}".format(DB_NAME))          
                    SQL_statement = 'SELECT TRAVEL_ID , DEPARTURE , DEPARTURE_TIME , ARRIVAL , ARRIVAL_TIME, COST \
                        FROM `HORIZON_TRAVELS_FOR_AIR`  LEFT JOIN `COST_TABLE_FOR_AIR` on TRAVEL_ID = COST_ID \
                        WHERE TRAVEL_ID = %s;'
                    args = (travelid,)
                    dbcursor.execute(SQL_statement,args)
                    print('SELECT statement executed successfully.')             
                    rows = dbcursor.fetchall()                                    
                    dbcursor.close()              
                    conn.close() #Connection must be closed
                    return render_template('show_retrieved_travel_data.html', resultset=rows)
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid travel ID received')
            return render_template('retrieving_data.html')  

@app.route("/show_all_travel", methods =["GET", "POST"])
def show_all_value():
    if request.method == "GET":
        conn = getConnection()
        if conn != None:
               dbcursor = conn.cursor()
               dbcursor.execute("USE {}".format(DB_NAME))
               SQL_statement = "SELECT * FROM `HORIZON_TRAVELS_FOR_AIR`;"
               dbcursor.execute(SQL_statement)
               print("Select statement successfully executed")
               rows = dbcursor.fetchall()
               dbcursor.close()
               conn.close()
               return render_template("show_all_travel.html", resultset=rows)
        else:
               print("DB connection error")
               return ("DB connection error")
    else:
        print("DB func error")
        return("DB func error")

               
@app.route('/add_travel', methods=['POST', 'GET'])
def add_tutor():     
    if request.method == 'GET':
        travelid = request.args.get('travelid')
        departure = request.args.get('departure')
        arrival = request.args.get('arrival')
        departuretime = request.args.get('departuretime')
        arrivaltime = request.args.get('arrivaltime')
        cost = request.args.get('cost')

        if travelid != None and departure != None and departure != None :
            conn = getConnection()
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()    #Creating cursor object   
                    dbcursor.execute("USE {}".format(DB_NAME) )       
                    SQL_statement = 'INSERT INTO `HORIZON_TRAVELS_FOR_AIR` VALUES (%s, %s, %s, %s, %s);'
                    args = (travelid,departure,departuretime,arrival,arrivaltime )
                    dbcursor.execute(SQL_statement,args)
                    print('INSERT statement executed successfully.') 

                    # route = (departure +"-"+ arrival)

                    SQL_statement2 = 'INSERT INTO `COST_TABLE_FOR_AIR` (COST_ID, COST) VALUES (%s, %s);'
                    args2 = (travelid,cost)
                    dbcursor.execute(SQL_statement2,args2)
                    print('INSERT statement executed successfully.') 
                    conn.commit()                                
                    dbcursor.close()              
                    conn.close() #Connection must be closed
                    return render_template('adding_to_travel_data.html', message="Flight succesfully added")
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid travel id received')
            return render_template('adding_to_travel_data.html')

@app.route('/update_travel/', methods=['POST', 'GET'])
def update_travel(): 
    #update travel
    if request.method == 'GET':
        travelid = request.args.get('travelid')
        departure = request.args.get('departure')
        departure_time = request.args.get('departuretime')
        arrival = request.args.get('arrival')
        arrival_time = request.args.get('arrivaltime')
        cost = request.args.get('costid')
        # print('Travel id is: ', travelid)
        if travelid != None:
            conn = getConnection()          
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()    #Creating cursor object 
                    dbcursor.execute("USE {}".format(DB_NAME))
                    if departure != None :          
                        SQL_statement = "UPDATE `HORIZON_TRAVELS_FOR_AIR` SET DEPARTURE = %s WHERE TRAVEL_ID = %s;"
                        args = (departure, travelid )
                        dbcursor.execute(SQL_statement,args)                    
                        print('UPDATE statement executed successfully.')
                        conn.commit()
                        dbcursor.close()              
                        conn.close()
                        return render_template('adding_to_travel_data.html', message="Flight detail succesfully updated")
                    if departure_time != None :          
                        SQL_statement = "UPDATE `HORIZON_TRAVELS_FOR_AIR` SET DEPARTURE_TIME = %s WHERE TRAVEL_ID = %s;"
                        args = (departure_time, travelid )
                        dbcursor.execute(SQL_statement,args)                    
                        print('UPDATE statement executed successfully.')               
                    #rows = dbcursor.fetchall()                     
                        conn.commit()
                        dbcursor.close()              
                        conn.close()
                        return render_template('adding_to_travel_data.html', message="Flight detail succesfully updated")
                    if arrival != None :          
                        SQL_statement = "UPDATE `HORIZON_TRAVELS_FOR_AIR` SET ARRIVAL = %s WHERE TRAVEL_ID = %s;"
                        args = (arrival, travelid )
                        dbcursor.execute(SQL_statement,args)                    
                        print('UPDATE statement executed successfully.')               
                    #rows = dbcursor.fetchall()                     
                        conn.commit()
                        dbcursor.close()              
                        conn.close()
                        return render_template('adding_to_travel_data.html', message="Flight detail succesfully updated")
                    if arrival_time != None :          
                        SQL_statement = "UPDATE `HORIZON_TRAVELS_FOR_AIR` SET ARRIVAL_TIME= %s  WHERE TRAVEL_ID = %s;"
                        args = (arrival_time, travelid )
                        dbcursor.execute(SQL_statement,args)                    
                        print('UPDATE statement executed successfully.')               
                    #rows = dbcursor.fetchall()                     
                        conn.commit()
                        dbcursor.close()              
                        conn.close()
                        return render_template('adding_to_travel_data.html', message="Flight detail succesfully updated")
                    if cost != None :          
                        SQL_statement = "UPDATE `COST_TABLE_FOR_AIR` SET COST = %s WHERE COST_ID = %s;"
                        args = (cost, travelid )
                        dbcursor.execute(SQL_statement,args)                    
                        print('UPDATE statement executed successfully.')  
                                     
                    #rows = dbcursor.fetchall()                     
                        conn.commit()
                        dbcursor.close()              
                        conn.close() #Connection must be closed
                        return render_template('adding_to_travel_data.html', message="Flight detail succesfully updated")
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid travel id received')
            return render_template('adding_to_travel_data.html') 

@app.route('/drop_travel', methods=['POST', 'GET'])
def drop_travel():
    #fetch from database with where clause  
    if request.method == 'GET':
        travelid = request.args.get('travelid')
        if travelid != None:
            conn = getConnection()
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()    #Creating cursor object  
                    dbcursor.execute ("USE {}".format(DB_NAME))          
                    SQL_statement = 'DELETE from `HORIZON_TRAVELS_FOR_AIR` WHERE TRAVEL_ID = %s;'
                    args = (travelid,)
                    dbcursor.execute(SQL_statement,args)
                    SQL_statement2 = 'DELETE from `COST_TABLE_FOR_AIR` WHERE COST_ID = %s;'
                    args2 = (travelid,)
                    dbcursor.execute(SQL_statement2,args2)
                    print('DROP statement executed successfully.') 
                    conn.commit()                                              
                    dbcursor.close()              
                    conn.close() #Connection must be closed
                    return render_template('adding_to_travel_data.html', message="Flight succesfully deleted")
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid travel ID received')
            return render_template('adding_to_travel_data.html')  

@app.route('/drop_users', methods=['POST', 'GET'])
def drop_users():
    #fetch from database with where clause  
    if request.method == 'POST':
        username = request.form['username']
        if username!= None:
            conn = getConnection()
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()  
                    dbcursor.execute ("USE {}".format(DB_NAME))          
                    SQL_statement = 'SELECT * from `USERS` WHERE username = %s;'
                    args = (username,)
                    dbcursor.execute(SQL_statement,args)  #Creating cursor object
                    rows = dbcursor.fetchall()
                    if dbcursor.rowcount < 1:
                        return render_template("adding_to_travel_data.html", message = "User does not exist")  
                    else:
                        SQL_statement2 = 'DELETE from `USERS` WHERE username = %s;'
                        args2 = (username,)
                        dbcursor.execute(SQL_statement2,args2)
                        print('DELETE statement executed successfully.') 
                        conn.commit()                                              
                        dbcursor.close()              
                        conn.close() #Connection must be closed
                        return render_template('adding_to_travel_data.html')
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid username received')
            return render_template('adding_to_travel_data.html') 

@app.route('/drop_booking', methods=['POST', 'GET'])
def drop_booking():
    #fetch from database with where clause  
    if request.method == 'POST':
        bookingid = request.form['bookingid']
        if bookingid != None:
            conn = getConnection()
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()    #Creating cursor object 
                    dbcursor.execute ("USE {}".format(DB_NAME))          
                    SQL_statement = 'SELECT DEPARTURE_DATE, TOTAL_FARE from `BOOKINGS_TABLE` WHERE UNIQUE_BOOKING_ID = %s;'
                    args = (bookingid,)
                    dbcursor.execute(SQL_statement,args)
                    print('SELECT statement executed successfully.')
                    row = dbcursor.fetchone()
                    # print (row)
                
                    if dbcursor.rowcount < 1:
                        return render_template("view_or_cancel_booking.html", message ="Booking not available")
                    
                    else:
                        departuredate = row[0]
                        departuredate = datetime.strptime(departuredate, '%Y-%m-%d')
                        departuredate = timestamp_microsecond(departuredate)
                        time = (departuredate - timenow )

                        SQL_statement = 'DELETE from `BOOKINGS_TABLE` WHERE UNIQUE_BOOKING_ID = %s;'
                        args = (bookingid,)
                        dbcursor.execute(SQL_statement,args)
                        conn.commit() 
                        print('DELETE statement executed successfully.') 

                        if ( (time < 5184000000000 and time > 5011200000000)):
                            return render_template('view_or_cancel_booking.html', message='Booking successfully deleted')
                        

                        elif ( (time <= 5011200000000 and 2592000000000  < time) ): 
                            cancellationfee = (float(row[1]) * 0.5)
                            return render_template("cancellation_fee.html", fee = cancellationfee, message='Booking successfully deleted')
                        
                        elif (time <= 2592000000000): 
                            dbcursor.close()              
                            conn.close() #Connection must be closed
                            cancellationfee = (float(row[1]) * 1.0)
                            return render_template("cancellation_fee.html", fee = cancellationfee, message='Booking successfully deleted')
                    # return render_template('v_or_c_bookings2', message='Booking successfully deleted')
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid Booking ID received')
            return (url_for('Cancel_booking') )
        
@app.route('/drop_account', methods=['POST', 'GET'])
def drop_account():
    #fetch from database with where clause  
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username != None:
            conn = getConnection()
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()    #Creating cursor object 
                    dbcursor.execute ("USE {}".format(DB_NAME))          
                    SQL_statement = 'SELECT password_hash from `USERS` WHERE username = %s;'
                    args = (username,)
                    dbcursor.execute(SQL_statement, args)
                    row = dbcursor.fetchone()
                    if sha256_crypt.verify(request.form['password'], row[0]):          
                        SQL_statement = 'DELETE from `USERS` WHERE username = %s;'
                        args = (username,)
                        dbcursor.execute(SQL_statement,args)
                        print('DELETE statement executed successfully.') 
                        conn.commit()                                              
                    dbcursor.close()              
                    conn.close() #Connection must be closed
                    return redirect( url_for('departurecity', message='Account successfully deleted'))
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid Username received')
            return (url_for('departurecity') )
        
@app.route('/show_booking', methods=['POST', 'GET'])
def show_booking():
    #fetch from database with where clause  
    if request.method == 'POST':
        bookingid = request.form['bookingid']
        if bookingid != None:
            conn = getConnection()
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()    #Creating cursor object  
                    dbcursor.execute ("USE {}".format(DB_NAME))          
                    SQL_statement = 'SELECT BOOKINGS_ID, DEPARTURE_CITY, DEPARTURE_DATE, ARRIVAL_CITY, \
                        RETURN_DATE, FLIGHT_CLASS, NUMBER_OF_SEATS, TOTAL_FARE  \
                        from `BOOKINGS_TABLE` WHERE UNIQUE_BOOKING_ID = %s;'
                    args = (bookingid,)
                    dbcursor.execute(SQL_statement,args)
                    print('SELECT statement executed successfully.') 
                    rows = dbcursor.fetchall()                                            
                    dbcursor.close()              
                    conn.close() #Connection must be closed
                    return render_template( 'show_booking_details.html',resultset = rows )
                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid Booking ID received')
            return (url_for('v_or_c_bookings') )

@app.route('/update_password/', methods=['POST', 'GET'])
def update_password(): 
    #update travel
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        oldpassword = request.form['oldpassword']
        # print('Travel id is: ', travelid)
        if username != None:
            conn = getConnection()          
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()
                    # oldpassword = sha256_crypt.hash((str(oldpassword)))  
                    dbcursor.execute("USE {}".format(DB_NAME))
                    SQL_statement = "SELECT password_hash FROM `USERS` WHERE username = %s;"
                    args = (username,)
                    dbcursor.execute(SQL_statement,args)
                    data = dbcursor.fetchone()
                    if sha256_crypt.verify(request.form['oldpassword'], str(data[0])):
                        password = sha256_crypt.hash((str(password)))     #Creating cursor object 
                        dbcursor.execute("USE {}".format(DB_NAME))      
                        SQL_statement = "UPDATE `USERS` SET password_hash = %s WHERE username = %s;"
                        args = (password, username, )
                        dbcursor.execute(SQL_statement,args)                    
                        print('UPDATE statement executed successfully.')               
                        #rows = dbcursor.fetchall()                     
                        conn.commit()
                        dbcursor.close()              
                        conn.close() #Connection must be closed
                        return redirect(url_for('one_way_ticket', message='Password successfully updated'))
                    else:
                        print('Invalid Credentials')
                        return redirect(url_for("updtpass"))

                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid travel id received')
            return redirect(url_for("one_way_ticket"))
    else:
        return("Method not allowed")
    

@app.route('/forgot_password/', methods=['POST', 'GET'])
def forgot_password(): 
    #update travel
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        oldpassword = request.form['oldpassword']
        # print('Travel id is: ', travelid)
        if username != None:
            conn = getConnection()          
            if conn != None:    #Checking if connection is None
                if conn.is_connected(): #Checking if connection is established
                    print('MySQL Connection is established')                          
                    dbcursor = conn.cursor()
                    # oldpassword = sha256_crypt.hash((str(oldpassword)))  
                    dbcursor.execute("USE {}".format(DB_NAME))
                    SQL_statement = "SELECT password_hash FROM `USERS` WHERE username = %s;"
                    args = (username,)
                    dbcursor.execute(SQL_statement,args)
                    data = dbcursor.fetchone()
                    if sha256_crypt.verify(request.form['oldpassword'], str(data[0])):
                        password = sha256_crypt.hash((str(password)))     #Creating cursor object 
                        dbcursor.execute("USE {}".format(DB_NAME))      
                        SQL_statement = "UPDATE `USERS` SET password_hash = %s WHERE username = %s;"
                        args = (password, username, )
                        dbcursor.execute(SQL_statement,args)                    
                        print('UPDATE statement executed successfully.')               
                        #rows = dbcursor.fetchall()                     
                        conn.commit()
                        dbcursor.close()              
                        conn.close() #Connection must be closed
                        return redirect(url_for('login', message='Password successfully changed'))
                    else:
                        print('Invalid Credentials')
                        return redirect(url_for("updtpass"))

                else:
                    print('DB connection Error')
                    return 'DB Connection Error'
            else:
                print('DB Connection Error')
                return 'DB Connection Error'
        else:
            print('Invalid travel id received')
            return redirect(url_for("one_way_ticket"))
    else:
        return("Method not allowed")

# @app.route('/sql/', methods=['POST', 'GET'])
# def sql(): 
#     #update travel
#     if request.method == 'GET':
#         sql = request.args.get('sql')
#         if sql != None:
#             conn = getConnection()          
#             if conn != None:    #Checking if connection is None
#                 if conn.is_connected(): #Checking if connection is established
#                     print('MySQL Connection is established')                          
#                     dbcursor = conn.cursor()    #Creating cursor object 
#                     dbcursor.execute("USE {}".format(DB_NAME))           
#                     SQL_statement = (sql)
#                     dbcursor.execute(SQL_statement)                    
#                     print('sql statement executed successfully.')             
#                     #rows = dbcursor.fetchall()                     
#                     dbcursor.close()              
#                     conn.close() #Connection must be closed
#                     return render_template('showsql.html')
#                 else:
#                     print('DB connection Error')
#                     return 'DB Connection Error'
#             else:
#                 print('DB Connection Error')
#                 return 'DB Connection Error'
#         else:
#             print('Invalid travel id received')
#             return render_template('adding_to_travel_data.html')

    
@app.route("/departurecity2", methods=['POST', 'GET'])
def departurecity2():
    conn = getConnection()
    form={}
    error = ''
    try:	
        if request.method == "POST":            
            username = request.form['username']
            password = request.form['password']            
            form = request.form
            
            if username != None and password != None:  #check if un or pw is none          
                conn = getConnection()
                if conn != None:    #Checking if connection is None                    
                    if conn.is_connected(): #Checking if connection is established                        
                        print('MySQL Connection is established')                          
                        dbcursor = conn.cursor()  #Creating cursor object   
                        dbcursor.execute("USE {}".format(DB_NAME))                                            
                        dbcursor.execute("SELECT password_hash, usertype FROM USERS WHERE username = %s;", (username,))
                                                                           
                        data = dbcursor.fetchone()
                        #print(data[0])
                        if dbcursor.rowcount < 1: #this mean no user exists                         
                            error = "User / password does not exist, try logging in again"
                            return render_template("Log in page.html", error=error)
                        else:                            
                             #extracting password       
                            # verify passowrd hash and password received from user                                                             
                            if sha256_crypt.verify(request.form['password'], str(data[0])):                                
                                session['logged_in'] = True     #set session variables
                                session['username'] = request.form['username']
                                session['usertype'] = str(data[1])                          
                                print("You are now logged in")
                                if session['usertype'] == 'admin':                               
                                    return render_template("admin.html")
                                else:
                                    SQL_statement = ("SELECT DISTINCT DEPARTURE FROM `HORIZON_TRAVELS_FOR_AIR`;") 
		                            #print('SELECT statement executed successfully.')  
                                    dbcursor.execute(SQL_statement)          
                                    rows = dbcursor.fetchall()                                    
                                    dbcursor.close()              
                                    conn.close() #Connection must be closed
                                    cities = []
                                    for city in rows:
                                        city = str(city).strip("(")
                                        city = str(city).strip(")")
                                        city = str(city).strip(",")
                                        city = str(city).strip("'")
                                        cities.append(city)
                                    return render_template("One way ticket.html", departureslist=cities)
                            else:
                                error = "Invalid credentials username/password, try again." 
                                return render_template("Log in page.html", error=error)                              
                                gc.collect()
                else:                    
                    print('Connection error')
            else:
                return render_template("Log in page.html", form=form, error=error)
        else:            
            return render_template("Log in page.html", error=error)
    except Exception as e:                
        error = str(e) + " <br/> Invalid credentials, try again."
        return render_template("Log in page.html", form = form, error = error)   
  
    if conn != None:    #Checking if connection is None         
        print("MySQL Connection is established")                          
        
    
    else:
        print("DB connection Error")
        return "DB Connection Error"
	
@app.route ('/returncity/', methods = ['POST', 'GET'])
def ajax_returncity():   
    if request.method == 'GET':
        deptcity = request.args.get('q')
        conn = getConnection()      
        if conn != None:    #Checking if connection is None         
            print('MySQL Connection is established')                          
            dbcursor = conn.cursor()    #Creating cursor object  
            dbcursor.execute("USE {}".format(DB_NAME))          
            dbcursor.execute("SELECT DISTINCT ARRIVAL FROM `HORIZON_TRAVELS_FOR_AIR` WHERE DEPARTURE = %s;", (deptcity,))   
			#print('SELECT statement executed successfully.')             
            rows = dbcursor.fetchall()
            total = dbcursor.rowcount                                    
            dbcursor.close()              
            conn.close() #Connection must be closed			
            return jsonify(returncities=rows, size=total)
        else:
            print('DB connection Error')
            return jsonify(returncities='DB Connection Error')
    else:
     return render_template("/") 

@app.route ('/selectBooking', methods = ['POST', 'GET'])
def selectBooking(): 
  
    if request.method == 'POST':
		#print('Select booking initiated')
        departcity = request.form['departureslist']
        arrivalcity = request.form['arrivalslist']
        outdate = request.form['outdate']
        returndate = request.form['returndate']
        adultseats = request.form['adultseats']
        childseats = request.form['childseats']
        bookingclass = request.form["bookingclass"]
        totalseats = int(adultseats) + int(childseats)

        conn = getConnection()
        if conn != None:
            
            dbcursor = conn.cursor() 
            dbcursor.execute("USE {}".format(DB_NAME))
            dbcursor.execute('SELECT * FROM `HORIZON_TRAVELS_FOR_AIR` LEFT JOIN `SEATS_TABLE` ON TRAVEL_ID = SEAT_ID\
                 WHERE DEPARTURE = %s AND ARRIVAL = %s;',(departcity, arrivalcity) )
            print("SELECT STATEMENT SUCCESSFUL")
            rows2 = dbcursor.fetchone()
            seats_available = int(rows2[6])
            print(seats_available)
            seats_left = (int(seats_available) - totalseats)

            print(seats_left)

            if (seats_left < 0): 
                return redirect(url_for("one_way_ticket", message="Sorry. No seats available anymore"))

            else:

                lookupdata = [departcity, arrivalcity, outdate, returndate, adultseats, childseats, bookingclass]
		        #print(lookupdata)
                #Checking if connection is None         
                print('MySQL Connection is established')                          
                dbcursor = conn.cursor()    #Creating cursor object
                dbcursor.execute("USE {}".format(DB_NAME))               
                dbcursor.execute('SELECT * FROM `HORIZON_TRAVELS_FOR_AIR` LEFT JOIN `COST_TABLE_FOR_AIR` on TRAVEL_ID = COST_ID WHERE DEPARTURE = %s AND ARRIVAL = %s;', (departcity, arrivalcity))   
		        #	print('SELECT statement executed successfully.')   

                outdate  = datetime.strptime(outdate, '%Y-%m-%d')

                outdte = timestamp_microsecond(outdate)

                time = (outdte - timenow )
                # time = (datetime.now() - timedelta(5)) 
                print(time)
                rows = dbcursor.fetchall()
                datarows=[]			
                for row in rows:
                    data = list(row)
                    if ( (time <= 7776000000000 and 6912000000000  <= time) and bookingclass == "Business"):
                        fare_ = float(float(row[6]) * 0.2)
                        fare = ((((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) * 2) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)

                    elif ( (time <= 6825600000000 and 5184000000000  <= time) and bookingclass == "Business"): 
                        fare_ = float(float(row[6]) * 0.1)                  
                        fare = ((((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) * 2) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif ( (time <= 5097600000000 and 3888000000000  <= time) and bookingclass == "Business"): 
                        fare_ = float(float(row[6]) * 0.05)                  
                        fare = ((((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats)))* 2) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif ( (time <= 7776000000000 and 6912000000000  <= time) and bookingclass == "Economy"):
                        fare_ = float(float(row[6]) * 0.2)
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)

                    elif ( (time <= 6825600000000 and 5184000000000  <= time) and bookingclass == "Economy"):                   
                        fare_ = float(float(row[6]) * 0.1)                  
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif ( (time <= 5097600000000 and 3888000000000  <= time) and bookingclass == "Economy"):                   
                        fare_ = float(float(row[6]) * 0.05)                  
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif (bookingclass == "Business"):
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) * 2)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif (bookingclass == "Economy"):
                        fare = ((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats)))
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                dbcursor.close()               
                conn.close() #Connection must be closed
			    #print(datarows)
			    #print(len(datarows))			
                return render_template('booking_details.html', resultset=datarows, lookupdata=lookupdata)
        else:
                print('DB connection Error')
                return redirect(url_for('hello'))
    else:
        return redirect(url_for('one_way_ticket', message="No seats available"))

@app.route ('/selectBooking2/', methods = ['POST', 'GET'])
def selectBooking2():        
    if request.method == 'POST':
		#print('Select booking initiated')
        departcity = request.form['departureslist']
        arrivalcity = request.form['arrivalslist']
        outdate = request.form['outdate']
        returndate = request.form['returndate']
        adultseats = request.form['adultseats']
        childseats = request.form['childseats']
        bookingclass = request.form["bookingclass"]

        lookupdata = [departcity, arrivalcity, outdate, returndate, adultseats, childseats, bookingclass]
	    #print(lookupdata)
        conn = getConnection()
        if conn != None:    #Checking if connection is None         
                print('MySQL Connection is established')                          
                dbcursor = conn.cursor()    #Creating cursor object
                dbcursor.execute("USE {}".format(DB_NAME))               
                dbcursor.execute('SELECT * FROM `HORIZON_TRAVELS_FOR_AIR` LEFT JOIN `COST_TABLE_FOR_AIR` on TRAVEL_ID = COST_ID WHERE DEPARTURE = %s AND ARRIVAL = %s;', (departcity, arrivalcity))   
		        #	print('SELECT statement executed successfully.')   

                totalseats = int(adultseats) + int(childseats)

                outdate  = datetime.strptime(outdate, '%Y-%m-%d')

                outdte = timestamp_microsecond(outdate)

                time = (outdte - timenow )
 
                print(time)
                rows = dbcursor.fetchall()
                datarows=[]	
                travelid = rows[0]	
                print(travelid)	
                for row in rows:
                    data = list(row)
                    if ( (time <= 7776000000000 and 6912000000000  <= time) and bookingclass == "Business"):
                        fare_ = float(float(row[6]) * 0.2)
                        fare = ((((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) * 2) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)

                    elif ( (time <= 6825600000000 and 5184000000000  <= time) and bookingclass == "Business"): 
                        fare_ = float(float(row[6]) * 0.1)                  
                        fare = ((((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) * 2) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif ( (time <= 5097600000000 and 3888000000000  <= time) and bookingclass == "Business"): 
                        fare_ = float(float(row[6]) * 0.05)                  
                        fare = ((((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats)))* 2) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif ( (time <= 7776000000000 and 6912000000000  <= time) and bookingclass == "Economy"):
                        fare_ = float(float(row[6]) * 0.2)
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)

                    elif ( (time <= 6825600000000 and 5184000000000  <= time) and bookingclass == "Economy"):                   
                        fare_ = float(float(row[6]) * 0.1)                  
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif ( (time <= 5097600000000 and 3888000000000  <= time) and bookingclass == "Economy"):                   
                        fare_ = float(float(row[6]) * 0.05)                  
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) - fare_)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif (bookingclass == "Business"):
                        fare = (((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats))) * 2)
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)
                    elif (bookingclass == "Economy"):
                        fare = ((float(row[6]) * float(adultseats)) + (float(row[6]) * 0.5 * float(childseats)))
				        #print(fare)
                        data.append(fare)
				        #print(data)
                        datarows.append(data)

                dbcursor.close()               
                conn.close() #Connection must be closed
			    #print(datarows)
			    #print(len(datarows))			
                return render_template('booking_details2.html', resultset=datarows, lookupdata=lookupdata)
        else:
            print('DB connection Error')
            return redirect(url_for('hello'))
    else:
            return redirect(url_for('departurecity', message="No seats available"))

	
@app.route ('/booking_confirm/', methods = ['POST', 'GET'])
def booking_confirm():
    if request.method == 'POST':		
		#print('booking confirm initiated')
        journeyid = request.form['bookingid']		
        departcity = request.form['deptcity']
        arrivalcity = request.form['arrivcity']
        outdate = request.form['outdate']
        returndate = request.form['returndate']
        adultseats = request.form['adultseats']
        childseats = request.form['childseats']
        totalfare = request.form['totalfare']
        cardnumber = request.form['cardnumber']
        bookingclass = request.form['bookingclass']

        print("okay")

        totalseats = int(adultseats) + int(childseats)
        bookingdata = [journeyid, departcity, arrivalcity, outdate, returndate, adultseats, childseats, totalfare, bookingclass]
        #print(bookingdata)
        conn = getConnection()
        if conn != None:    #Checking if connection is None         
            print('MySQL Connection is established')                          
            dbcursor = conn.cursor()    #Creating cursor object
            dbcursor.execute("USE {}".format(DB_NAME))
            dbcursor.execute('SELECT * FROM `HORIZON_TRAVELS_FOR_AIR` LEFT JOIN `SEATS_TABLE` ON TRAVEL_ID = SEAT_ID\
                                 WHERE DEPARTURE = %s AND ARRIVAL = %s;',(departcity, arrivalcity) )
            print("SELECT STATEMENT SUCCESSFUL")
            rows2 = dbcursor.fetchone()
            seats_available = int(rows2[6])
            print(seats_available)
            seats_left = (int(seats_available) - totalseats)
            print(seats_left)

            if (seats_left < 0): 
                    return redirect(url_for('one_way_ticket', message="Sorry. No seats available anymore")) 
            else:
                    dbcursor.execute('UPDATE `SEATS_TABLE` SET `AVAILABLE_SEATS` = %s WHERE `SEAT_ID` = %s;', (seats_left, journeyid ))
                    conn.commit()

                    #dbcursor.execute('SELECT AUTO_INCREMENT - 1 FROM information_schema.TABLES WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s;', ('TEST_DB', 'bookings'))  
                    dbcursor.execute('SELECT LAST_INSERT_ID();')  
                    #print('SELECT statement executed successfully.')             
                    rows = dbcursor.fetchone()
			        #print ('row count: ' + str(dbcursor.rowcount))
                    bookingid = rows[0]

                    unique_booking_id = sha256_crypt.hash((str(bookingid))) 
                    unique_booking_id = unique_booking_id[-10:-1]

                    dbcursor.execute('SELECT Curdate();')  
                    row = dbcursor.fetchone()
                    curdate = row[0]  
                    print(curdate)

                    dbcursor.execute('INSERT INTO `BOOKINGS_TABLE` (DEPARTURE_CITY, DEPARTURE_DATE, ARRIVAL_CITY, RETURN_DATE, FLIGHT_CLASS, NUMBER_OF_SEATS, TOTAL_FARE, UNIQUE_BOOKING_ID, DATE_OF_BOOKING) VALUES \
			        (%s, %s, %s, %s, %s, %s, %s , %s, %s );', (departcity, outdate, arrivalcity, returndate, bookingclass, totalseats, totalfare, unique_booking_id, curdate))   
                    print('Booking statement executed successfully.')              

                    # dbcursor.excute('INSERT INTO `BOOKINGS_TABLE` (DATE_OF_BOOKING) VALUE (%s) WHERE BOOKINGS_ID = %s;',( curdate, journeyid))     

                    conn.commit()

                    bookingdata.append(bookingid)
                    bookingdata.append(unique_booking_id)

                    dbcursor.execute('SELECT * FROM `HORIZON_TRAVELS_FOR_AIR` WHERE TRAVEL_ID = %s;', (journeyid,))   			
                    rows = dbcursor.fetchall()
                    deptTime = rows[0][2]
                    arrivTime = rows[0][4]
                    bookingdata.append(deptTime)
                    bookingdata.append(arrivTime)
			        #print(bookingdata)
			        #print(len(bookingdata))
                    cardnumber = cardnumber[-4:-1]
                    print(cardnumber)
                    dbcursor.execute
                    dbcursor.close()              
                    conn.close() #Connection must be closed
                    return render_template("booking_confirmation.html", resultset=bookingdata, cardnumber=cardnumber)
        else:
            print('DB connection Error')
            return redirect(url_for('departurecity'))

# @app.route ('/dumpsVar/', methods = ['POST', 'GET'])
# def dumpVar():
# 	if request.method == 'POST':
# 		result = request.form
# 		output = "<H2>Data Received: </H2></br>"
# 		output += "Number of Data Fields : " + str(len(result))
# 		for key in list(result.keys()):
# 			output = output + " </br> " + key + " : " + result.get(key)
# 		return output
# 	else:
# 		result = request.args
# 		output = "<H2>Data Received: </H2></br>"
# 		output += "Number of Data Fields : " + str(len(result))
# 		for key in list(result.keys()):
# 			output = output + " </br> " + key + " : " + result.get(key)
# 		return output  

@app.route ('/farereport', methods = ['POST', 'GET'])
def report():   
    if request.method == 'GET':
        conn = getConnection()      
        if conn != None:    #Checking if connection is None         
            print('MySQL Connection is established')                          
            dbcursor = conn.cursor()    #Creating cursor object  
            dbcursor.execute("USE {}".format(DB_NAME))          
            dbcursor.execute("SELECT SUM(TOTAL_FARE) as 'Total_cost_of_bookings_made' FROM `BOOKINGS_TABLE`;")   
			#print('SELECT statement executed successfully.')             
            rows = dbcursor.fetchall()                                                
            total = rows[0]
            total = str(total).strip("(")
            total = str(total).strip(")")
            total = str(total).strip(",")
            dbcursor.close() 
            conn.close() 
            return render_template("farereport.html", fare = total)	
        else:
            print('DB connection Error')
            return render_template('')
    else:
     return render_template("/") 
    
@app.route ('/journeysales', methods = ['POST', 'GET'])
def journeysales():   
    if request.method == 'GET':
        departurecity = request.args.get('departurecity')
        arrivalcity = request.args.get('arrivalcity')
        print(departurecity)
        print(arrivalcity )
        conn = getConnection()      
        if conn != None:    #Checking if connection is None         
            print('MySQL Connection is established')                          
            dbcursor = conn.cursor()    #Creating cursor object  
            dbcursor.execute("USE {}".format(DB_NAME))          
            dbcursor.execute("SELECT COUNT(DEPARTURE_CITY AND ARRIVAL_CITY) , \
                            SUM(TOTAL_FARE)  FROM `BOOKINGS_TABLE` \
                            WHERE DEPARTURE_CITY = %s AND ARRIVAL_CITY = %s; ",(departurecity, arrivalcity))   
			#print('SELECT statement executed successfully.')             
            rows = dbcursor.fetchall()  
            resultset = rows                          
            if dbcursor.rowcount < 1:
                return render_template("adding_to_travel_data.html", message="No flights were made") 
            else:
                dbcursor.close()
                conn.close()  
                return render_template("journeysales.html", resultset = resultset, departure = departurecity, arrival = arrivalcity)                   
        else:
            print('DB connection Error')
            return render_template('adding_to_travel_data.html')
    else:
     return render_template("adding_to_travel_data.html", message="Method not allowed") 

@app.route ('/monthlyrevenue', methods = ['POST', 'GET'])
def monthlyrevenue(): 
    if request.method == 'POST':
        monthnum = request.form['monthnum'] 
        conn = getConnection()      
        if conn != None:    #Checking if connection is None         
            print('MySQL Connection is established')                          
            dbcursor = conn.cursor()    #Creating cursor object  
            dbcursor.execute("USE {}".format(DB_NAME))  
            dbcursor.execute("SELECT MONTH(DATE_OF_BOOKING) FROM `BOOKINGS_TABLE` WHERE MONTH(DATE_OF_BOOKING) = %s;", (monthnum,))
            row = dbcursor.fetchone()
            

            if dbcursor.rowcount < 1:
                return render_template("adding_to_travel_data.html", message="No Flights were made in this month")
            else:

                dbcursor.execute("SELECT SUM(TOTAL_FARE) FROM `BOOKINGS_TABLE` WHERE MONTH(DATE_OF_BOOKING) = %s;", (monthnum,))
			#print('SELECT statement executed successfully.') 
            #             
                rows = dbcursor.fetchall()  
                                            
                revenue = rows[0]

                revenue = str(revenue).strip("(")
                revenue= str(revenue).strip(")")
                revenue = str(revenue).strip(",")

                dbcursor.close() 
                conn.close() 
                return render_template("monthlyrevenue.html", revenue = revenue, month = monthnum)	
        else:
            print('DB connection Error')
            return render_template('')
    else:
     return render_template("/") 

@app.route ('/routereport', methods = ['POST', 'GET'])
def routereport():   
    if request.method == 'GET':
        departurecity = request.args.get('departurecity')
        arrivalcity = request.args.get('arrivalcity')
        month = request.args.get('month')

        print(departurecity)
        print(arrivalcity )
        conn = getConnection()      
        if conn != None:    #Checking if connection is None         
            print('MySQL Connection is established')                          
            dbcursor = conn.cursor()    #Creating cursor object  
            dbcursor.execute("USE {}".format(DB_NAME))          
            dbcursor.execute("SELECT MONTH(DATE_OF_BOOKING) AS MONTH, COUNT(DEPARTURE_CITY AND ARRIVAL_CITY) AS COUNT \
                             FROM `BOOKINGS_TABLE` WHERE DEPARTURE_CITY= %s AND ARRIVAL_CITY = %s \
                            AND MONTH(DATE_OF_BOOKING) = %s GROUP BY Month(DATE_OF_BOOKING) ; ",(departurecity, arrivalcity, month))   
			#print('SELECT statement executed successfully.')             
            rows = dbcursor.fetchone()  
                               
            if dbcursor.rowcount < 1:
                return render_template("adding_to_travel_data.html", message="No flights were made") 
            else:
                count = rows[1] 
                count= str(count).strip("(")
                count = str(count).strip(")")
                count = str(count).strip(",")
                count = str(count).strip("'")
                dbcursor.close()
                conn.close()  
                return render_template("routereport.html", count = count, departure = departurecity, arrival = arrivalcity)                   
        else:
            print('DB connection Error')
            return render_template('adding_to_travel_data.html')
    else:
     return render_template("adding_to_travel_data.html", message="Method not allowed") 


@app.route('/logout')
def logout():
   # remove the username from the session if it is there
   session.pop('username', None)
   # session.clear() can also be used to clear all session variables
   return redirect(url_for('departurecity'))
   #return redirect(url_for('index'))

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:            
            print("You need to login first")
            #return redirect(url_for('login', error='You need to login first'))
            return render_template('Log in page.html', error='You need to login first')    
    return wrap

def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if ('logged_in' in session) and (session['usertype'] == 'admin'):
            return f(*args, **kwargs)
        else:            
            print("You need to login first as admin user")
            #return redirect(url_for('login', error='You need to login first as admin user'))
            return render_template('Log in page.html', error='You need to login first as admin user')    
    return wrap

def standard_user_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if ('logged_in' in session) and (session['usertype'] == 'standard'):
            return f(*args, **kwargs)
        else:            
            print("You need to login first as standard user")
            #return redirect(url_for('login', error='You need to login first as standard user'))
            return render_template('Log in page.html', error='You need to login first as standard user')    
    return wrap

@app.route("/logout/")
@login_required
def log_out():    
    session.clear()    #clears session variables
    print("You have been logged out!")
    gc.collect()
    return redirect(url_for("log_in_and_sign_up"))

# @app.route('/generateadminreport/')
# @login_required
# @admin_required
# def generate_admin_report():
#     print('admin reports')
#     #here you can generate required data as per business logic
#     return """
#         <h1> this is admin report for {} </h1>
#         <a href='admin.html')> Go to Admin Features page </a>
#     """.format(session['username'])

# @app.route('/generateuserrecord/')
# @login_required
# @standard_user_required
# def generate_user_record():
#     print('User records')
#     #here you can generate required data as per business logic
#     return """
#         <h1> this is User record for user {} </h1>
#         <a href='randomuser.html')> Go to User Features page </a>
#     """.format(session['username'])

if __name__ == "__main__":
    app.run(port= "5002", debug = True) 

